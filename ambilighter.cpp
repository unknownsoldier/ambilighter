#include <QtGui>
#include <QColor>
#include <QPainter>
#include <QPaintEvent>
#include <QImage>
#include <QBasicTimer>

#include <CoreFoundation/CoreFoundation.h>
#include <ApplicationServices/ApplicationServices.h>

#include <OpenGL/OpenGL.h>

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <OpenGL/glext.h>

#include "ambilighter.h"

//#define DEBUG
//#define PAINT

Ambilighter::Ambilighter(QWidget *parent) : QWidget(parent)
{
    //image = new QImage();
    //image->load(":res/tv.png");

    QRect screenSize = QApplication::desktop()->screenGeometry();
    screenW = screenSize.width();
    screenH = screenSize.height();

    // 32 led
    areasX = 11;
    areasY = 7;

    areas = ((areasX - 1) * 2) + ((areasY - 1) * 2);

    blockW = floor(screenW / areasX);
    blockH = floor(screenH / areasY);

#ifdef DEBUG
    printf("Screen size: %i x %i\n", screenW, screenH);
    printf("Block size: %i x %i\n", blockW, blockH);
#endif

    displayBlockW = blockW * 0.275;
    displayBlockH = blockH * 0.275;

    // 32 led
    int ledList[] = {
       11,12,13,14,15,16,17,18,19,20,21,
       10,                           22,
        9,                           23,
        8,                           24,
        7,                           25,
        6,                           26,
        5, 4, 3, 2, 1, 0,31,30,29,28,27,
    };

#ifdef DEBUG
    printf("areas: %i\n", areas);
#endif

    coordinates = (pointCoordinates*) malloc(areas * sizeof(pointCoordinates));

    short i, j = 0;

    short row = 0;
    short column = 0;

    short blocks = ((screenW * screenH) / (blockW * blockH));
    short blocksX = screenW / blockW;
    short blocksY = screenH / blockH;

    for (i = 0; i < blocks; i++) {
      column = floor(i % blocksX);
      row    = floor(i / blocksX );

      // skip columns which are outside the edges, we won't need them.
      if (row > 0 && row < (blocksY - 1) && column > 0 && column < (blocksX - 1) ) { continue; }

      coordinates[ledList[j]].x = round(blockW * column);
      coordinates[ledList[j]].y = round(blockH * row);

#ifdef DEBUG
      printf("row: %i column: %i blocksY: %i blocksX: %i\n", row, column, blocksY-1, blocksX-1);
#endif

      if ((row == 0 && column == 0) || (row == (blocksY - 1) && column == (blocksX - 1)))
      {
          coordinates[ledList[j]].width = blockW;
          coordinates[ledList[j]].height = blockH;
      }
      else if (row == 0 || row == (blocksY - 1))
      {
          coordinates[ledList[j]].width = blockW;
          coordinates[ledList[j]].height = blockH * 0.75;
      } else if (column == 0  || column == (blocksX - 1)) {
          coordinates[ledList[j]].width = blockW * 0.75;
          coordinates[ledList[j]].height = blockH;
      }

      j++;
    }

    // set up the buffer and fill it.
    bufferSize = 6 + (areas * 3);
    buffer = new unsigned char[bufferSize];

    bzero(buffer, bufferSize);

    buffer[0] = 'A';                          // Magic word
    buffer[1] = 'd';
    buffer[2] = 'a';
    buffer[3] = (areas - 1) >> 8;             // LED count high byte
    buffer[4] = (areas - 1) & 0xff;           // LED count low byte
    buffer[5] = buffer[3] ^ buffer[4] ^ 0x55; // Checksum

    float f;

    // Pre-compute gamma correction table for LED brightness levels:
    for(i = 0; i < 256; i++) {
      f           = pow((float) i / 255.0, 1.2);

      gamma[i][0] = (char) (f * 255.0); // 235
      gamma[i][1] = (char) (f * 255.0);
      gamma[i][2] = (char) (f * 255.0); // 185
    }

    arduino = new Arduino();
    arduino->connect();
    arduino->send(buffer, bufferSize);

    resize(352, 200);
    setWindowTitle(tr("Ambilighter"));

    connect(QApplication::desktop(), SIGNAL(resized(int)), this, SLOT(onScreenResize()));

    timer.start(40, this);   // 40 = 25fps, 33 = 30fps, 17 = 60fps

    this->initGL();
}

Ambilighter::~Ambilighter()
{
    this->cleanGL();
}

void Ambilighter::paintEvent(QPaintEvent *)
{
#ifdef PAINT
    QPainter painter(this);

    short r = 0, g = 0, b = 0, j = 0;
    int i;

    for (i = 6; i < bufferSize;)
    {
        r = (short) buffer[i++];
        g = (short) buffer[i++];
        b = (short) buffer[i++];

        painter.setPen(QColor(r, g, b));
        painter.setBrush(QColor(r, g, b));

        QRectF rectangle(coordinates[j].x * 0.275, coordinates[j].y * 0.275, displayBlockW, displayBlockH);
        painter.drawRect(rectangle);

        j++;
    }
    //painter.drawImage(6, 0, *image);
#endif
}

// slot function to get new sceen size on change
void Ambilighter::onScreenResize()
{
    QRect screenSize = QApplication::desktop()->screenGeometry();

    screenW = screenSize.width();
    screenH = screenSize.height();
}

void Ambilighter::timerEvent(QTimerEvent *event)
{
    short i;

    for (i = 0; i < areas; i++)
        this->analyzeArea(i);

#ifdef PAINT
    this->update();
#endif
    arduino->send(buffer, bufferSize);
}

void Ambilighter::initGL()
{
#ifdef DEBUG
    printf("Initializing OpenGL\n");
#endif
    CGDirectDisplayID glDisplay = kCGDirectMainDisplay;
//    CGLContextObj glContextObj;
    CGLPixelFormatObj pixelFormatObj;

    long numPixelFormats;

    CGLPixelFormatAttribute glAttributes[] =
    {
        kCGLPFAFullScreen,
        kCGLPFADisplayMask,
        (CGLPixelFormatAttribute) CGDisplayIDToOpenGLDisplayMask(glDisplay),
        (CGLPixelFormatAttribute) 0
    };

    if ( glDisplay == kCGNullDirectDisplay )
        glDisplay = CGMainDisplayID();

    /* Build a full-screen GL context */
    CGLChoosePixelFormat( glAttributes, &pixelFormatObj, (GLint *)&numPixelFormats );

    if ( pixelFormatObj == NULL )    // No full screen context support
        return;

    CGLCreateContext( pixelFormatObj, NULL, &glContextObj ) ;
    CGLDestroyPixelFormat( pixelFormatObj ) ;

    if ( glContextObj == NULL )
        return;

    CGLSetCurrentContext( glContextObj ) ;
    CGLSetFullScreen( glContextObj ) ;

    glReadBuffer(GL_FRONT);

    glFinish(); /* Finish all OpenGL commands */

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ROW_LENGTH, 0);
    glPixelStorei(GL_PACK_SKIP_ROWS, 0);
    glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
    glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);

    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_3D);
    glDisable(GL_DEPTH_TEST);

    glShadeModel(GL_FLAT);
}

void Ambilighter::cleanGL()
{
#ifdef DEBUG
    printf("Cleaning up OpenGL\n");
#endif

    if ( glContextObj != NULL )
    {
#ifdef DEBUG
        printf("Releasing OpenGL Context\n");
#endif
        /* Get rid of GL context */
        CGLSetCurrentContext( NULL );
        CGLClearDrawable( glContextObj ); // disassociate from full screen
        CGLDestroyContext( glContextObj ); // and destroy the context
    }
}

void Ambilighter::analyzeScreen()
{
    short i;

    for (i = 0; i < areas; i++)
        this->analyzeArea(i);
}

void Ambilighter::analyzeArea(int index)
{
    //GLenum errCode;
    //const GLubyte *errString;

    unsigned int i = 0, count = 0;
    unsigned int r = 0, g = 0, b = 0;

    GLint x = coordinates[index].x;
    GLint y = ((screenH - blockH) - coordinates[index].y);
    GLint width  = coordinates[index].width;
    GLint height = coordinates[index].height;

    unsigned int pixelBufferSize = width * height;
    unsigned int pixelBuffer[pixelBufferSize];

    glReadPixels(x, y, width, height,
                 GL_BGRA, // GL_RGB
                 GL_UNSIGNED_INT_8_8_8_8_REV, // GL_UNSIGNED_BYTE for Intel! http://lists.apple.com/archives/quartz-dev/2006/May/msg00100.html
                 &pixelBuffer);
/*
    if ((errCode = glGetError()) != GL_NO_ERROR) {
        errString = gluErrorString(errCode);
       fprintf (stderr, "glReadPixels OpenGL Error: %s\n", errString);
    }
*/
    if (pixelBuffer != NULL)
    {
        for (i = 0; i < pixelBufferSize; i += 20)
        {
            b += pixelBuffer[i] >>  0 & 0xFF;
            g += pixelBuffer[i] >>  8 & 0xFF;
            r += pixelBuffer[i] >> 16 & 0xFF;

            count++;
        }

        index = (index * 3) + 6;

        r = (r / count) * 0.96;
        g = (g / count) * 1.00;
        b = (b / count) * 0.52;

        buffer[index++] = gamma[r][0];
        buffer[index++] = gamma[g][1];
        buffer[index++] = gamma[b][2];
    }

#ifdef DEBUG
    //printf("index: %i x,y: %i %i\n", index, x, y);
    //printf("RGB: %i %i %i\n", buffer[index][0], buffer[index][1], buffer[index][2]);
#endif
}
