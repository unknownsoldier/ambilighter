#ifndef AMBILIGHTER_H
#define AMBILIGHTER_H

#define STATIC

#include <QWidget>
#include <QBasicTimer>
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>

#include "arduino.h"

class QWidget;
class QImage;

class Ambilighter : public QWidget
{

Q_OBJECT

public:
    Ambilighter(QWidget *parent = 0);
    ~Ambilighter();

    void initGL();
    void cleanGL();
    void analyzeScreen();
    void analyzeArea(int index);

    CGLContextObj glContextObj;

    unsigned char *buffer;

    unsigned char gamma[256][3];

    int bufferSize;
    short areas;

    typedef struct structCoordinates {
        GLint x;
        GLint y;
        GLint width;
        GLint height;
    } pointCoordinates;
    pointCoordinates* coordinates;

private:
    QBasicTimer timer;

    QImage *image;

    Arduino *arduino;

    short screenAreas;
    short pixelOffset;
    short screenW;
    short screenH;
    short areasX;
    short areasY;

    GLint blockW;
    GLint blockH;

    short displayBlockW;
    short displayBlockH;

protected:
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);

private slots:
    void onScreenResize();
};

#endif // AMBILIGHTER_H
