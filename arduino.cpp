#include "arduino.h"

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>
#include <math.h>

Arduino::Arduino(QWidget *parent) : QWidget(parent)
{
    fd      = -1;
    errors  = 0;
}

Arduino::~Arduino() {
    if (fd > 0) {
        int close(fd);
    }
}

void Arduino::connect() {
    if (fd < 0) {
#ifdef DEBUG
        printf("Connecting to Arduino... ");
#endif
        if ((fd = open("/dev/tty.usbserial-A70063mG", O_RDWR | O_NOCTTY | O_NONBLOCK)) > 0)
        {
#ifdef DEBUG
            printf(" connected.\n");
#endif
            tcgetattr(fd, &tty);
            tty.c_iflag       = INPCK;
            tty.c_lflag       = 0;
            tty.c_oflag       = 0;
            tty.c_cflag       = CREAD | CS8 | CLOCAL;
            tty.c_cc[ VMIN ]  = 0;
            tty.c_cc[ VTIME ] = 0;
            cfsetispeed(&tty, B57600);
            cfsetospeed(&tty, B57600);
            tcsetattr(fd, TCSANOW, &tty);
        } else {
#ifdef DEBUG
            printf(" connection failed.\n");
#endif
        }
    }
}

void Arduino::send(unsigned char *buffer, int bufferSize) {
    int i, bytesSent, bytesToGo;

    if (errors < 5)
    {
        if (fd > 0) {
            tcdrain(fd);

            for (bytesSent = 0, bytesToGo = bufferSize; bytesToGo > 0;) {
                if ((i = write(fd, &buffer[bytesSent], bytesToGo)) > 0) {
                    bytesToGo -= i;
                    bytesSent += i;
                } else {
                    errors++;
                }
            }
        } else {
            errors++;
        }
    } else {
        errors = 0;

        this->disconnect();
        this->connect();
    }
}

void Arduino::disconnect() {
    if (fd > 0) {
        int close(fd);
    }
}
