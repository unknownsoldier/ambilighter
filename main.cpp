#include <QApplication>

#include "ambilighter.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Ambilighter w;
    w.show();

    return a.exec();
}
