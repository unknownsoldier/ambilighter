#-------------------------------------------------
#
# Project created by QtCreator 2011-10-09T12:01:27
#
#-------------------------------------------------

QT       += core gui

TARGET = ambilighter
TEMPLATE = app


SOURCES += main.cpp \
    ambilighter.cpp \
    arduino.cpp

HEADERS  += \
    ambilighter.h \
    arduino.h

CONFIG += static

macx{
    # MacOS version using libusb and hidapi codes
    # MacOS version using Qt grabWindow(..) for grab colors
    CONFIG += x86_64
    LIBS +=-framework IOKit -framework CoreFoundation -framework ApplicationServices -framework OpenGL
}

RESOURCES += \
    ambilighter.qrc

FORMS += \
    ambilighter.ui












