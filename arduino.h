#ifndef SERIAL_H
#define SERIAL_H

#include <QWidget>
#include <termios.h>

class Arduino : public QWidget
{
    Q_OBJECT

public:
    Arduino(QWidget *parent = 0);
    ~Arduino();

    void connect();
    void send(unsigned char *buffer, int bufferSize);
    void disconnect();

private:
    struct termios tty;
    int fd, errors;

signals:

public slots:

};

#endif // SERIAL_H
